/*
 * IPv6 Neighbor Advertisement Faker
 * Copyright (C) 2012, Christoph Grenz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IPv6 Neighbor Advertisement Faker is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * -----
 *
 * Written mainly to circumvent a limitation on IPv6 subnets on servers hosted by OVH
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>

#undef __FAVOR_BSD

#include <netinet/in.h>
#include <net/if.h>
#include <netpacket/packet.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netinet/if_ether.h>

#include <sys/types.h>
#include <ifaddrs.h>

/// Prefix length of subnet to use
#define PREFIXLEN 64

/// Set to 1 or 0 to set/unset the R-Flag in sent advertisements
#define ISROUTER 1

/// Global variable to check if the program should be terminated.
/// Also used to save the causing signal number
static volatile sig_atomic_t aborting;

/// Global to check if in verbose mode
int verbose;

/// Device name buffer
static char device[IFNAMSIZ];
/// Local address buffers
static struct in6_addr my_addr, my_lladdr, my_subnet;
/// MAC address buffer
static u_int8_t my_hwaddr[ETH_ALEN];
/// Socket handle
static int sock;

/// IPv6 pseudo header structure for ICMPv6 checksum calculation
struct ipv6_pseudo_hdr {
	struct in6_addr source;
	struct in6_addr destination;
	u_int32_t ulp_length;
	u_int32_t zero: 24,
		nexthdr: 8;
};

/// Signal handler
/// Saves the signal number in `aborting' for graceful termination.
static void signal_handler(sig_atomic_t sig)
{
	aborting = -sig;
}

/// Pretty print a buffer in hexadecimal
/// Used for debugging purposes.
static void printBufferHex ( const void *buffer, unsigned int size ) __attribute__((nonnull, unused));
static void printBufferHex ( const void *buffer, unsigned int size )
{
	assert(buffer != NULL);
	unsigned int i;
	printf ("%s","\x1b[1;32m");
	for(i = 0; i<size; ++i)
	{
		printf("%02x ",((unsigned char*)(buffer))[i]);
	}
	printf ("%s","\x1b[0m\n");
}

/// ICMPv6 checksum algorithm
unsigned short csum (const unsigned short * buf, int nwords) __attribute__((nonnull, pure, warn_unused_result));
unsigned short csum (const unsigned short * buf, int nwords)
{
	assert(buf != NULL);
	assert(nwords > 0 && "Cannot calculate the checksum of zero words");
	const unsigned short *ptr = buf;
	unsigned long sum;
	for (sum = 0; nwords > 0; nwords--)
		sum += *ptr++;
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	return ~sum;
}

/// Calculate prefix length from a netmask
int getprefixlen ( const struct in6_addr *netmask ) __attribute__((nonnull, pure, warn_unused_result));
int getprefixlen ( const struct in6_addr *netmask )
{
	assert(netmask != NULL);
	int i, j, length;
	unsigned int addr[4];
	length = 128;
	for (i=0; i < 4; ++i) addr[i] = ntohl(netmask->s6_addr32[i]);
	for (i=3; i >= 0; --i)
		for (j=0; j < 32; ++j)
		{
			if (addr[i]&1)
				return length;
			length--;
			addr[i] >>= 1;
		}
	return length;
}

/// Remove the host part from an IPv6 address
void chophostpart ( struct in6_addr *addr, unsigned int prefixlen )
{
	assert(addr != NULL);
	unsigned int i;
	for (i=0; i<16; ++i)
		if ( prefixlen < (8*(i+1)) )
		{
			if ( prefixlen > 8*i+1 )
				addr->s6_addr[i] &= ((1<<8)-1) ^ ((1<<(8-(prefixlen%8)))-1);
			else
				addr->s6_addr[i] = 0;
		}
}

/// Compare two addresses up to a prefix length
/// Used to check if a subnet contains an address.
int addrcmp(const struct in6_addr *a, const struct in6_addr *b, int prefixlen) __attribute__((nonnull, pure, warn_unused_result));
int addrcmp(const struct in6_addr *a, const struct in6_addr *b, int prefixlen)
{
	assert(a != NULL);
	assert(b != NULL);
	int memlen1 = (int)prefixlen/8;
	if (memlen1 > 0)
	{
		int i = memcmp(a, b, memlen1);
		if (i != 0) return i;
	}

	int memlen2 = ceil((double)prefixlen/8);
	if (memlen2 == memlen1) return 0;
	char _a, _b;
	_a = a->s6_addr[memlen1] >> (8-(prefixlen % 8));
	_b = a->s6_addr[memlen1] >> (8-(prefixlen % 8));
	if (_a < _b) return -1;
	else if (_a > _b) return 1;
	return 0;
}

/// Check if an IPv6 address is link local (i.e. is in fe80::/10)
int islinklocal ( const struct in6_addr *addr ) __attribute__((nonnull, pure, warn_unused_result));
int islinklocal ( const struct in6_addr *addr )
{
	assert(addr != NULL);
	return (addr->s6_addr[0] == 0xfe) && (addr->s6_addr[1] >> 6 == 0x2 );
}

/// Pretty print an IPv6 address to stdout
void printIPv6( const struct in6_addr *addr )
{
	assert(addr != NULL);
	char buffer[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, addr, buffer, sizeof(buffer));
	printf("%s", buffer);
}

/// Return the interface index of a named interface
/// Needs a socked descriptor to run.
int getIfaceIndex( int socketfd, const char *iface )
{
	struct ifreq ifr;
	size_t if_name_len = strlen(iface);

	if (if_name_len < sizeof(ifr.ifr_name)) {
		memcpy(ifr.ifr_name, iface, if_name_len);
		ifr.ifr_name[if_name_len]='\0';
	} else return 0;

	if (ioctl(socketfd, SIOCGIFINDEX, &ifr)==-1) {
		perror("Error while getting device index");
		return 0;
	}
	return ifr.ifr_ifindex;
}

/// Determines the MAC address, the first link-local address and the first
/// global address with a given prefix length for a given interface
void getIfaceAddresses( const char* iface, int prefix_len, struct in6_addr *first_addr, struct in6_addr *ll_addr, u_int8_t hw_addr[ETH_ALEN])
{
	int a_found = (first_addr == 0), l_found = (ll_addr == 0), d_found = (hw_addr == 0);
	struct ifaddrs *list = 0, *ptr = 0;
	if (getifaddrs(&list) == 0)
	{
		ptr = list;
		while (ptr != 0)
		{
			if ((strcmp(iface, ptr->ifa_name) == 0) )
			{

			 	if (ptr->ifa_addr->sa_family == AF_INET6)
			 	{
					struct sockaddr_in6 *addr = (struct sockaddr_in6*)ptr->ifa_addr;
					struct sockaddr_in6 *mask = (struct sockaddr_in6*)ptr->ifa_netmask;
					if (!islinklocal(&addr->sin6_addr))
					{
						if (((!prefix_len) || (getprefixlen(&mask->sin6_addr) == prefix_len)) && (!a_found))
						{
							*first_addr = addr->sin6_addr;
							a_found = 1;
						}
					}
					else if (!l_found)
					{
						*ll_addr = addr->sin6_addr;
						l_found = 1;
					}
				}
				else if ((!d_found) && (ptr->ifa_addr->sa_family == AF_PACKET))
				{
					memcpy(hw_addr, (void*)(ptr->ifa_addr->sa_data)+10, 6);
					d_found = 1;
				}
			}
			ptr = ptr->ifa_next;
		}
	}
	freeifaddrs(list);
	int i;
	struct in6_addr tmp;
	for (i=0;i<4;++i) tmp.s6_addr32[i] = 0;
	if (!a_found) *first_addr = tmp;
	if (!l_found) *ll_addr = tmp;

	u_int8_t tmp2[ETH_ALEN] = {0,0,0,0,0,0};
	if (!d_found) memcpy(hw_addr, tmp2, 6);
	return;
}

/// Create a neighbor advertisement in the given buffer.
void createNeighborAdvert(struct nd_neighbor_advert *, struct in6_addr, int, int, int) __attribute__((nonnull));
void createNeighborAdvert(struct nd_neighbor_advert *buf, struct in6_addr addr, int router_flag, int unicast_flag, int override_flag)
{
	assert(buf != NULL);
	buf->nd_na_type = ND_NEIGHBOR_ADVERT;
	buf->nd_na_code = 0;
	buf->nd_na_cksum = 0;
	buf->nd_na_flags_reserved = 0;
	if (router_flag) buf->nd_na_flags_reserved |= ND_NA_FLAG_ROUTER;
	if (unicast_flag) buf->nd_na_flags_reserved |= ND_NA_FLAG_SOLICITED;
	if (override_flag) buf->nd_na_flags_reserved |= ND_NA_FLAG_OVERRIDE;
	buf->nd_na_target = addr;
}

/// Neighbor solicitation handler
void handleNDP_NS(struct sockaddr_in6 from, void * buffer, unsigned int size)
{
	assert(buffer != NULL);
	// Assert that given packet is big enough for a NDP solicitation
	assert(size >= sizeof(struct nd_neighbor_solicit));
	//if (verbose) printBufferHex(buffer, size);

	// do not answer requests from localhost
	if (memcmp(&from, &my_addr, sizeof(struct in6_addr)) == 0) return;
	if (memcmp(&from, &my_lladdr, sizeof(struct in6_addr)) == 0) return;

	// Get structure in buffer
	struct nd_neighbor_solicit *ns = buffer;
	if (verbose)
	{
		printf("%s", "Neighbor Solicitation for ");
		printIPv6( &ns->nd_ns_target );
		printf("%s", " from ");
		printIPv6( &from.sin6_addr );
		printf("%s", " received.\n");
		fflush(stdout);
	}

	// Check if target is in my prefix
	if (addrcmp(&ns->nd_ns_target, &my_subnet, PREFIXLEN) != 0)
		return;
	// Ignore requests for localhost as they are handled by the kernel
	if (memcmp(&ns->nd_ns_target,&my_addr, sizeof(struct in6_addr)) == 0)
		return;

	// Inner scope for the real work
	{
		// Create raw IPv6 socket using data link layer sockets
		// to allow arbitrary source address
		int sendsock = socket(AF_PACKET, SOCK_DGRAM, htons(ETH_P_IPV6));
		if (sendsock == -1) {
			perror("creating socket for sending failed");
			return;
		}

		// Create interface descriptor for binding to a device
		int ifidx = getIfaceIndex(sendsock, device);
		struct sockaddr_ll binddev = {
			.sll_family = AF_PACKET,
			.sll_protocol = htons(ETH_P_IPV6),
			.sll_ifindex = ifidx,
			.sll_hatype = 0,
			.sll_pkttype = 0,
			.sll_halen = 0,
			.sll_addr = "\0\0\0\0\0\0\0\0"
		};

		// Bind to device
		if (bind(sendsock, (struct sockaddr*)&binddev, sizeof(binddev)) == -1) {
			perror("bind to device for sending failed");
			close(sendsock);
			return;
		}

		// Assemble pseudo IPv6 packet (for checksum calculation)
		int na_p_size = ceil((double)(sizeof(struct ipv6_pseudo_hdr)+sizeof(struct nd_neighbor_advert)+sizeof(struct nd_opt_hdr)+ETH_ALEN)/2);
		unsigned short na_p_buf[na_p_size];
		memset(na_p_buf, 0, sizeof(na_p_buf));
		struct ipv6_pseudo_hdr *p_header = (void*)na_p_buf;
		struct nd_neighbor_advert *p_response = (void*)na_p_buf+sizeof(struct ipv6_pseudo_hdr);
		struct nd_opt_hdr *p_opt_hdr = (void*)na_p_buf+sizeof(struct ipv6_pseudo_hdr)+sizeof(struct nd_neighbor_advert);
		u_int8_t *p_hw_addr = (void*)na_p_buf+sizeof(struct ipv6_pseudo_hdr)+sizeof(struct nd_neighbor_advert)+sizeof(struct nd_opt_hdr);

		// Fill IPv6 pseudo header
		p_header->source = ns->nd_ns_target;
		p_header->destination = from.sin6_addr;
		p_header->ulp_length = htons(sizeof(na_p_buf) - sizeof(struct ipv6_pseudo_hdr));
		p_header->zero = 0;
		p_header->nexthdr = IPPROTO_ICMPV6;

		// Generate ICMPv6 packet content
		createNeighborAdvert(p_response, ns->nd_ns_target, ISROUTER, 1, 0);
		p_opt_hdr->nd_opt_type = ND_OPT_TARGET_LINKADDR;
		p_opt_hdr->nd_opt_len = 1;
		memcpy(p_hw_addr,my_hwaddr,6);

		// Assemble IPv6 packet
		int na_size = sizeof(struct ip6_hdr)+sizeof(struct nd_neighbor_advert)+sizeof(struct nd_opt_hdr)+ETH_ALEN;
		char na_buf[na_size];
		memset(na_buf, 0, sizeof(na_buf));
		struct ip6_hdr *header = (void*)na_buf;
		struct nd_neighbor_advert *response = (void*)na_buf+sizeof(struct ip6_hdr);
		/*struct nd_opt_hdr *opt_hdr = (void*)na_buf+sizeof(struct ip6_hdr)+sizeof(struct nd_neighbor_advert);
		u_int8_t *hw_addr = (void*)na_buf+sizeof(struct ip6_hdr)+sizeof(struct nd_neighbor_advert)+sizeof(struct nd_opt_hdr);*/

		// Fill IPv6 header
		header->ip6_flow = 0;
		header->ip6_vfc = 6 << 4;
		header->ip6_plen = htons(na_size - sizeof(struct ip6_hdr));
		header->ip6_nxt = IPPROTO_ICMPV6;
		header->ip6_hlim = 255;
		header->ip6_src = ns->nd_ns_target;
		header->ip6_dst = from.sin6_addr;

		// Fill in ICMPv6 packet content
		memcpy(response, p_response, sizeof(struct nd_neighbor_advert)+sizeof(struct nd_opt_hdr)+6);
		// Generate checksum
		response->nd_na_cksum = csum(na_p_buf, na_p_size);

		// Link-layer broadcast recipient address for sending
		struct sockaddr_ll recipient = {
			.sll_family = AF_PACKET,
			.sll_protocol = htons(ETH_P_IPV6),
			.sll_ifindex = ifidx,
			.sll_hatype = 0,
			.sll_pkttype = 0,
			.sll_halen = 6,
			.sll_addr = "\xFF\xFF\xFF\xFF\xFF\xFF"
		};

		if (verbose) printf("Sending fake advertisement... ");

		// Send response
		if ( sendto(sendsock, na_buf, na_size, MSG_CONFIRM, (struct sockaddr*)&recipient, sizeof(recipient)) == -1 ) {
			perror("Send failed");
		} else if (verbose)
			printf("Sent.\n");

		fflush(stdout);
		// Close send socket
		close(sendsock);
	}

}

int main(int argc, char** argv)
{
	char *progname = argv[0];
	verbose = 0;

	// -d: Daemonize
	if ((argc > 1) && (strcmp(argv[1], "-d") == 0))
	{
		argv++;
		argc--;
		if (daemon(0,0) == -1) {
			perror("Could not daemonize");
		}
	}

	// -v: Be verbose
	if ((argc > 1) && (strcmp(argv[1], "-v") == 0))
	{
		argv++;
		argc--;
		verbose=1;
	}

	if (argc != 2)
	{
		printf("Usage: %s [-d] [-v] <device>\n", progname);
		return 1;
	}

	// Copy device name argument into global device name variable
	strncpy(device, argv[1], IFNAMSIZ);
	device[IFNAMSIZ-1] = '\0';

	// Variable for early exit reasons.
	int r = 0;

	// Get the first IPv6 address for the given prefix length, the link-local
	// address and the MAC address of the specified device in one go.
	getIfaceAddresses(device, PREFIXLEN, &my_addr, &my_lladdr, my_hwaddr);

	// Create raw IPv6 socket using data link layer sockets
	// to listen to ICMPv6 Neighbor Requests
	// Using AF_INET6+SOCK_RAW+IPPROTO_ICMPV6 would be cleaner but as
	// we need a PF_PACKET socket to change the ALLMULTI interface flag
	// we just use it for everything.
	sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_IPV6));
	if (sock != -1)
	{
		struct ifreq oldifr, ifr;
		if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, &device, strlen(device)+1 ) == 0)
		{
			aborting = 0;

			/* Generate subnet from address*/
			my_subnet = my_addr;
			chophostpart(&my_subnet, PREFIXLEN);

			if (my_lladdr.s6_addr16[0] == 0) /* First two octets are 0000... This cannot be a correct link local address */
			{
				printf("Could not find a link-local address on %s! Is IPv6 enabled?\nAborting.\n", device);
				close(sock);
				exit(1);
			}

			if (my_subnet.s6_addr16[0] == 0) /* First two octets are 0000... This cannot be a correct global unicast address */
			{
				printf("Could not find a (non-link-local) /%i-subnet on %s!\nAborting.\n", PREFIXLEN, device);
				close(sock);
				exit(1);
			}

			// Handle most signals that would kill the process
			// for graceful shutdown
			struct sigaction new_action;
			new_action.sa_handler=signal_handler;
			sigemptyset(&new_action.sa_mask);
			new_action.sa_flags=0;
			sigaction(SIGHUP, &new_action, NULL);
			sigaction(SIGINT, &new_action, NULL);
			sigaction(SIGQUIT, &new_action, NULL);
			sigaction(SIGABRT, &new_action, NULL);
			sigaction(SIGPIPE, &new_action, NULL);
			sigaction(SIGTERM, &new_action, NULL);
			sigaction(SIGUSR1, &new_action, NULL);
			sigaction(SIGUSR2, &new_action, NULL);

			// Set interface to promiscous mode
			memset(ifr.ifr_name,0,IFNAMSIZ);
			strncpy(ifr.ifr_name, device, IFNAMSIZ);
			if(ioctl(sock, SIOCGIFFLAGS, &ifr)<0)
				perror("getting interface flags failed!");
			else
			{
				memcpy(&oldifr, &ifr, sizeof(ifr));
				if (!(ifr.ifr_flags & IFF_ALLMULTI))
				{
					ifr.ifr_flags |= IFF_ALLMULTI;
					if(ioctl(sock, SIOCSIFFLAGS, &ifr)<0)
						perror("Switching interface to promiscous multicast mode failed");
					else
						if (verbose) printf("Enabling promiscous multicast mode on %s\n", device);
				}
			}

			// set socket to all-multicast mode
			struct packet_mreq pr;
			memset(&pr, 0, sizeof(pr));
			ioctl(sock, SIOCGIFINDEX, &ifr);
			pr.mr_ifindex = ifr.ifr_ifindex;
			pr.mr_type = PACKET_MR_ALLMULTI;
			if(setsockopt(sock, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &pr, sizeof(struct packet_mreq)) < 0)
				perror("switching socket to promiscous multicast mode failed");

			if (!aborting)
			{
				printf("Using subnet ");
				printIPv6(&my_subnet);
				printf("/%i (from IPv6 address ", PREFIXLEN);
				printIPv6(&my_addr);
				printf(") on %s.\n", device);

				if (verbose) printf("%s", "Waiting for packets...\n");
				fflush(stdout);

				char buffer[sizeof(struct ether_header)+sizeof(struct ip6_hdr)+sizeof(struct nd_neighbor_solicit)];
				char buffer2[1024];
				struct sockaddr_in6 from;

				// Main Loop
				int r = 0;
				while (!aborting)
				{
					r = recv(sock, buffer, sizeof(buffer), 0/*, (struct sockaddr*)&from, &fromsize*/);
					if (r != -1)
					{
						recv(sock, buffer2, sizeof(buffer2), MSG_DONTWAIT);
						struct ip6_hdr *ipheader = (void*)buffer+sizeof(struct ether_header);
						from.sin6_family = AF_INET6;
						from.sin6_addr = ipheader->ip6_src;
						from.sin6_port = 0;
						from.sin6_flowinfo = 0;
						from.sin6_scope_id = 0;
						if (ipheader->ip6_nxt != IPPROTO_ICMPV6) continue;

						struct icmp6_hdr *header = (void*)ipheader+sizeof(struct ip6_hdr);
						//printf("Type: %u, code: %u\n", header->icmp6_type, header->icmp6_code);
						if ((header->icmp6_type == ND_NEIGHBOR_SOLICIT) && (header->icmp6_code == 0))
						{
							handleNDP_NS(from, (void*)buffer+sizeof(struct ether_header)+sizeof(struct ip6_hdr), sizeof(buffer)-sizeof(struct ether_header)-sizeof(struct ip6_hdr));
						}
					}
				}
				printf("\n");
			}
			shutdown(sock, SHUT_RDWR);
			if (!(oldifr.ifr_flags & IFF_ALLMULTI)) // Only reset interface flags if we changed them
			{
				if (verbose) printf("Disabling promiscous multicast mode on %s\n", device);
				if(ioctl(sock, SIOCSIFFLAGS, &oldifr)<0)
					perror("Switching back from promiscous multicast mode failed");
			}
		}
		else
			r = 2;
		close(sock);
	}
	else
		r = 1;

	if (r == 1)
	{
		perror("socket() failed");
		if (geteuid() != 0)
			printf("%s","This program needs root privileges!\n");
	}
	else if (r == 2)
	{
		perror("bind to interface failed");
	}
	fflush(stdout);
	// Resend signal if aborted because of signal
	if (aborting < 0) {
		int signal = -aborting;
		struct sigaction dfl_action = {
			.sa_handler=SIG_DFL,
			.sa_flags=0
		};
		sigaction(signal, &dfl_action, NULL); 
		raise(signal);
	}

	return r;
}
