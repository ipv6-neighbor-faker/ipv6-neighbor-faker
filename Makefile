# IPv6 Neighbor Advertisement Faker
# Copyright (C) 2012, Christoph Grenz
#
# IPv6 Neighbor Advertisement Faker is free software: you can
# redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# IPv6 Neighbor Advertisement Faker is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


CFLAGS ?= -Wall -Wextra -fwhole-program -Os
PREFIX = /usr/local

ipv6-neighbor-faker: ipv6-neighbor-faker.c
	$(CC) $(CFLAGS) "$<" -lm -o "$@"

clean:
	rm ipv6-neighbor-faker

install: ipv6-neighbor-faker
	install -m 0755 -o root -g root -s ipv6-neighbor-faker "$(PREFIX)"/sbin

uninstall:
	rm "$(PREFIX)"/sbin/ipv6-neighbor-faker

.PHONY: clean install uninstall
